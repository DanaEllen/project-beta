import React, { useState, useEffect } from 'react';

const VehicleModelForm = () => {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturerId, setManufacturerId] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(response => response.json())
            .then(data => setManufacturers(data.manufacturers));
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            name,
            picture_url: pictureUrl,
            manufacturer_id: manufacturerId
        };
        const url = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(url, fetchConfig);
        const result = await response.json();
        console.log(result);
        setName("");
        setPictureUrl("");
        setManufacturerId("");
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Model Name:
                <input type="text" value={name} onChange={(e) => setName(e.target.value)} required />
            </label>
            <label>
                Picture URL:
                <input type="url" value={pictureUrl} onChange={(e) => setPictureUrl(e.target.value)} required />
            </label>
            <label>
                Manufacturer ID:
                <select value={manufacturerId} onChange={(e) => setManufacturerId(e.target.value)} required>
                    <option value="">Choose a manufacturer</option>
                    {manufacturers.map((manufacturer) => (
                        <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                        </option>
                    ))}
                </select>
            </label>
            <input type="submit" value="Submit" />
        </form>
    );
}

export default VehicleModelForm;
