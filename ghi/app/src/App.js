import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturersList from './ManufacturersList';
import Nav from './Nav';
import ListVehicleModels from './VehicleModelsList';
import ManufacturerForm from './ManufacturerForm';
import { useState, useEffect } from 'react';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceAppointmentList from './ServiceAppointmentList';
import SalespeopleList from './SalespeopleList';
import CreateSalespersonForm from './CreateSalespersonForm';
import CustomerList from './CustomerList';
import CreateCustomerForm from  './CreateCustomerForm'
import SalesList from './SalesList';
import ServiceHistory from './ServiceHistory';
import SaleRecordForm from './SaleRecordForm';
import SalespersonHistory from './SalespersonHistory';


function App() {
  const [vehicleModels, setVehicleModels] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [automobiles, setAutomobiles] = useState([]); 
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [technicians, setTechnicians] = useState([]);


  const getVehicles = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const {models} = await response.json();
      setVehicleModels(models);
    } else {
      console.error("Error getting vehicle data");
    }
  }

  const getManufacturers = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const {manufacturers} = await response.json();
      setManufacturers(manufacturers)
    } else {
      console.error("An error occurred getting manufacturer data")
    }
  }

  const getAutomobiles = async () => { 
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const {autos} = await response.json();
      setAutomobiles(autos)
    } else {
      console.error("An error occurred getting automobile data")
    }
  }


  const getAppointments = async () => { 
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const {appointments} = await response.json();
      setAppointments(appointments)
    } else {
      console.error("An error occurred getting appointment data")
    }
  }    
  
  const getTechnicians = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/technicians/");
      if (response.ok) {
        const {technicians} = await response.json();
        setTechnicians(technicians);
      } else {
        console.error("Error getting technicians data");
      }
    } catch (error) {
      console.error("An error occurred during the fetch:", error);
    }
  }

  const getSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const {salespeople} = await response.json();
      setSalespeople(salespeople)
    } else {
      console.error("An error occurred getting salespeople data")
    }
  }

  const getCustomers = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const {customers} = await response.json();
      setCustomers(customers)
    } else {
      console.error("An error occurred getting customers data")
    }
  }

  const getSales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const {sales} = await response.json();
      setSales(sales)
    } else {
      console.error("An error occurred getting customers data")
    }
  }


  useEffect( () => {
    getManufacturers(); 
    getVehicles();
    getAutomobiles(); 
    getSalespeople();
    getCustomers();
    getSales();
    getAppointments();
    getTechnicians(); 
  }, []);

             

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="vehicle-models" element={<ListVehicleModels models={vehicleModels} />} />
          <Route path="vehicle-models/new" element={<VehicleModelForm />} />
          <Route path="manufacturers/" element={<ManufacturersList manufacturers={manufacturers}/>} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="automobiles/" element={<AutomobileList automobiles={automobiles}/>} />
          <Route path="automobile/new" element={<AutomobileForm automobiles={automobiles}/>} />
          <Route path="technicians/" element={<TechnicianList />} /> 
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="appointments" element={<ServiceAppointmentList appointments={appointments}/>} /> 
          <Route path="appointments/new" element={<ServiceAppointmentForm />} />
          <Route path="salespeople/" element={<SalespeopleList salespeople={salespeople}/>} />
          <Route path="salesperson/new" element={<CreateSalespersonForm />} />
          <Route path="customers/" element={<CustomerList customers={customers}/>} />
          <Route path="customers/new" element={<CreateCustomerForm />} />
          <Route path="sales/" element={<SalesList sales={sales}/>} />
          <Route path="service-history" element={<ServiceHistory appointments={appointments} />} />
          <Route path="saleshistory" element={<SalespersonHistory />} />
          <Route path="sales/new" element={<SaleRecordForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
