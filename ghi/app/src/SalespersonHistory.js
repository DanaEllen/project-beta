import React, { useEffect, useState } from "react";


const SalespersonHistory = (props) => {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');


    const handleSalespersonChange = (event) => {
        const { value } = event.target;
        setSalesperson(value);
    }

    
    const getSalesData = async () => {
        const salesResponse = await fetch("http://localhost:8090/api/sales/")
        const peopleResponse = await fetch("http://localhost:8090/api/salespeople/");
        if (salesResponse.ok && peopleResponse.ok) {
            const { sales } = await salesResponse.json();
            const { salespeople } = await peopleResponse.json();
            setSales(sales)
            setSalespeople(salespeople)
        }
    }

    useEffect(() => {
        getSalesData();
    }, [])

    if(salesperson) {
        return (
            <div>
                <h1 className="mb-3">Sales History</h1>
                <div className="mb-3">
                    <select onChange={handleSalespersonChange} required name="salesperson" label="Salesperson" id="salesperson" className="form-select" value={salesperson}>
                        <option value="">Select a Salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div>
                    <table className="history-table table table-striped">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sales.filter( sale => sale.salesperson.id === parseInt(salesperson)).map( sale => {
                                console.log(salesperson)
                                return (
                                    <tr key={sale.id}>
                                        <td>
                                            {sale.salesperson.first_name} {sale.salesperson.last_name}
                                        </td>
                                        <td>
                                            {sale.customer.first_name} {sale.customer.last_name}
                                        </td>
                                        <td>{sale.automobile.vin}</td>
                                        <td>{sale.price}</td>
                                    </tr>
                                )
                            })
                            }
                        </tbody>
                    </table>
                </div>
    
            </div>
        )
    } else {
        return (
            <div>
                <h1 className="mb-3">Sales History</h1>
                <div className="mb-3">
                    <select onChange={handleSalespersonChange} required name="salesperson" label="Salesperson" id="salesperson" className="form-select" value={salesperson}>
                        <option value="">Select a Salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <h3>Choose a sales person from the dropdown to view their sales history</h3>
            </div>
        )
    }  
}
export default SalespersonHistory;