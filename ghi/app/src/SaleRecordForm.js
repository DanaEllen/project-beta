import React, {useState, useEffect} from 'react';

const SaleRecordForm = () => {
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');
    const [auto, setAuto] = useState('');
    const [autos, setAutos] = useState([])
    const [customers, setCustomers] = useState([]);
    const [salespeople, setSaleseople] = useState([]);


    const handleAutoChange = (event) => {
        const { value } = event.target;
        setAuto(value)
    }

    const handleSalespersonChange = (event) => {
        const { value } = event.target;
        setSalesperson(value)
    }

    const handleCustomerChange = (event) => {
        const { value } = event.target;
        setCustomer(value)
    }

    const handlePriceChange = (event) => {
        const { value } = event.target;
        setPrice(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            price,
            auto,
            salesperson,
            customer
        }
        console.log("DATA", data)
        const url = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale)
            setPrice('');
            setAuto('');
            setSalesperson('');
            setCustomer('');
        }
    }

    const getSaleData = async () => {
        const autosResponse = await fetch("http://localhost:8100/api/automobiles/");
        const salespeopleResponse = await fetch("http://localhost:8090/api/salespeople/");
        const customersResponse = await fetch("http://localhost:8090/api/customers/");

        if (autosResponse.ok && salespeopleResponse.ok && customersResponse.ok) {

            const {autos} = await autosResponse.json();
            const {salespeople} = await salespeopleResponse.json();
            const {customers} = await customersResponse.json();
            
            setAutos(autos);
            setSaleseople(salespeople);
            setCustomers(customers);
            console.log(autos, customers, salespeople)
        }
    }

    useEffect(() => {
        getSaleData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} required placeholder="Price" type="text" name="price" id="price" className="form-control" value={price} />
                            <label htmlFor="price">Price $</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleAutoChange} required name="auto" id="auto" className="form-select" value={auto}>
                                <option value="">Select an Auto</option>
                                {autos.map(auto => {
                                    return (
                                        <option key={auto.vin} value={auto.vin}>
                                            {auto.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select" value={salesperson}>
                                <option value="">Select a Salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select" value={customer}>
                                <option value="">Select a Customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Add Sale</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SaleRecordForm;