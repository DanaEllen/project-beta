import React from "react";

const ManufacturersList = (props) => {
    return (
        <div>
            <h1>Manufacturers</h1>
            <table className="manufacturer-table table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {props.manufacturers.map( manufacturer => {
                        return (
                            <tr key={manufacturer.href}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default ManufacturersList