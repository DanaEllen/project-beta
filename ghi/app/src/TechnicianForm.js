import React, { useState } from 'react';

const TechnicianForm = () => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId
        };
        const url = "http://localhost:8080/api/technicians/"; 
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(url, fetchConfig);
        const result = await response.json();
        console.log(result);
        setFirstName("");
        setLastName("");
        setEmployeeId("");
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>
                First Name:
                <input type="text" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
            </label>
            <label>
                Last Name:
                <input type="text" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
            </label>
            <label>
                Employee ID:
                <input type="text" value={employeeId} onChange={(e) => setEmployeeId(e.target.value)} required />
            </label>
            <input type="submit" value="Submit" />
        </form>
    );
}

export default TechnicianForm;
