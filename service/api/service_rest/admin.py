from django.contrib import admin
from .models import AutomobileVO, Tech, Appointment

admin.site.register(AutomobileVO)
admin.site.register(Tech)
admin.site.register(Appointment)
