from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .Encoders import TechEncoder, AppointmentEncoder, AutomobileVOEncoder
from .models import AutomobileVO, Tech, Appointment


@require_http_methods(["GET", "POST"])
def list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": list(automobiles)},
            encoder=AutomobileVOEncoder,
        )
    else:  
        try:
            data = json.loads(request.body)
            automobile = AutomobileVO.objects.create(
                vin=data['vin'],
                sold=data.get('sold', False)
            )
            return JsonResponse(
                automobile,
                encoder=AutomobileVOEncoder,
                safe=False,
                status=201
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the automobile"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def create_automobile(request, pk):
    try:
        automobile = AutomobileVO.objects.get(id=pk)
    except AutomobileVO.DoesNotExist:
        return JsonResponse({"message": "Automobile does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            automobile,
            encoder=AutomobileVOEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        automobile.delete()
        return JsonResponse(
            {"success": "Automobile deleted!"},
            status=200
        )
    else:  # PUT
        try:
            data = json.loads(request.body)
            automobile.vin = data.get('vin', automobile.vin)
            automobile.sold = data.get('sold', automobile.sold)
            automobile.save()
            return JsonResponse(
                automobile,
                encoder=AutomobileVOEncoder,
                safe=False
            )
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON data"}, status=400)



@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        techs = Tech.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=TechEncoder,
        )
    else:  
        try:
            data = json.loads(request.body)
            tech = Tech.objects.create(
                first_name=data['first_name'],
                last_name=data['last_name'],
                employee_id=data['employee_id']
            )
            return JsonResponse(
                tech,
                encoder=TechEncoder,
                safe=False,
                status=201
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the tech"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def create_technician(request, pk):
    if request.method == "GET":
        try:
            tech = Tech.objects.get(id=pk)
            return JsonResponse(
                tech,
                encoder=TechEncoder,
                safe=False
            )
        except Tech.DoesNotExist:
            return JsonResponse({"message": "Tech does not exist"}, status=404)
    else:  # POST
        try:
            data = json.loads(request.body)
            tech = Tech.objects.create(
                first_name=data['first_name'],
                last_name=data['last_name'],
                employee_id=data['employee_id']
            )
            return JsonResponse(
                tech,
                encoder=TechEncoder,
                safe=False,
                status=201
            )
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON data"}, status=400)
        except Exception as e:
            return JsonResponse({"message": f"Error creating technician: {str(e)}"}, status=500)


@require_http_methods(["DELETE"])
def delete_technician(request, pk):
    try:
        tech = Tech.objects.get(id=pk)
        tech.delete()
        return JsonResponse(
            {"success": "Technician deleted!"},
            status=200
        )
    except Tech.DoesNotExist:
        return JsonResponse({"message": "Technician does not exist"}, status=404)
    except Exception as e:
        return JsonResponse({"message": f"Error deleting technician: {str(e)}"}, status=500)



@require_http_methods(["GET", "POST"])
def list_appointments(request):
    print("ooga booga")
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:  
        try:
            print("IN POST")
            data = json.loads(request.body)
            print(data)
            appointment = Appointment.objects.create(
                date_time=data['date_time'],
                reason=data['reason'],
                status=data['status'],
                vin=data['vin'],
                customer=data['customer'],
                technician_id=data['technician']
            )
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                status=201
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def create_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(pk=pk)
            appointment.delete()
            return JsonResponse({}, status=204)
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else:  
        try:
            data = json.loads(request.body)
            appointment = Appointment.objects.get(pk=pk)
            status = data.get('status', '')
            if status == 'canceled' or status == 'finished':
                appointment.status = status
                appointment.save()
                return JsonResponse({"status": status}, status=200)
            else:
                return JsonResponse({"message": "Invalid status"}, status=400)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def list_service_history(request):
    if request.method == "GET":
        appointments = Appointment.objects.filter(status__in=['finished', 'canceled'])
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,

            
        )


# notes on how/why i combined these- i realized i had done them all sperately and that wouldnt work because the project
# specified api_endpoints that would overlap, and it just looked bad and may have been 
# causing confusion in the localhost loading them

# most of the errors I was having seemed to be related to the way i made my encoders, and when i used the encoder file 
# in the inventory as a template it fixed it all! make sure to ask later if a seperate encoder file is best practice
 
