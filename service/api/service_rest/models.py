from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20)
    sold = models.BooleanField(default=False)


class Tech(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=20)
    

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50)
    vin = models.CharField(max_length=20)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(Tech, on_delete=models.CASCADE)
# adding a boolean field for VIP (such as) vip = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.date_time} - {self.customer} - {self.technician}"