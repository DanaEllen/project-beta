from django.urls import path
from .views import (
    list_technicians,
    create_technician,
    list_appointments,
    create_appointment,
    list_automobiles,
    create_automobile,
    list_service_history
)

urlpatterns = [
    path('technicians/', list_technicians, name='list_technicians'),
    path('technicians/new', create_technician, name='create_technician'), 
    path('appointments/', list_appointments, name='list_appointments'),
    path('appointments/new', create_appointment, name='create_appointment'),
    path('api/automobiles', list_automobiles, name='list_automobiles'),
    path('api/automobile/<int:pk>', create_automobile, name='create_automobile'),
    path('api/service-history/', list_service_history, name='list_service_history'),  
]
