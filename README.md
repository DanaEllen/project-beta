# CarCar

link to excalidraw diagram:
https://i.imgur.com/jvdOIHX.png


Team:

* Dana Blanchard - Service Microservice
* Naomi Campos - Sales Microservice

## Design
In this project, CarCar, we are completing a car dealership application that manages the sales and service departments, both working with the inventory.
We created RESTful APIs for all three microservices and called on those in the front end, allowing us to display the data on the DOM.

## Instructions to run the application
This application runs by building Docker containers and Docker images. Please have Docker Desktop downloaded in order to continue on with the instructions below:
​
#### Cloning the Repository
* Using terminal, change directories to a one where this file can be cloned 
* Type `git clone https://gitlab.com/DanaEllen/project-beta` into terminal
* CD into the newly cloned directory
​
#### Starting the application with Docker
* Once in the correct directory in your terminal, type the following commands once and hit enter after each one:
​
* `docker volume create beta-data`
* `docker-compose build`
* `docker-compose up`
​
## Navigation with URLs
#### The server uses `http://localhost:3000` as the Home Page. When entering that into your browser, you will see a navigation bar. Below is a breakdown of each URL and what feature it links to:

##### Vehicle Models:
​
| Feature          | URL          |
|:-----------------|:-------------|
|List vehicle models|http://localhost:3000/vehicle-models|
|Create vehicle models| http://localhost:3000/vehicle-models/new|
​
##### Automobiles:
​
| Feature          | URL          |
|:-----------------|:-------------|
|List automobiles| http://localhost:3000/automobiles/|
|Create automobiles| http://localhost:3000/automobile/new|
​
##### Manufacturers:
​
| Feature          | URL          |
|:-----------------|:-------------|
|List manufactureres|http://localhost:3000/manufacturers|
|Add a manufactureres|http://localhost:3000/manufacturers/new|
​
##### Service Microservice:
​
| Feature          | URL          |
|:-----------------|:-------------|
|Show list of technicians| http://localhost:3000/technicians/
|Create service appointments| http://localhost:3000/appointments/new|
|List service history based VIN| http://localhost:3000/services/new|
|Create a technicion| http://localhost:3000/technicians/new|
|Show list of technicion| http://localhost:3000/technicians/new|

## Service microservice - Dana Blanchard
​
I created an AutomobileVO, Techs (technicians), and Appointments as my models. Then I added a foreign key into my Appointment model to get access to my Technician model. The poller I created made objects for me by polling from the inventory API.
​
In the back-end for my Services Microservice under the views.py (path is service/api/service_rest/views.py) I first made class functions for my models, then views, and configured urls for each model to ensure the data that my microservice was handling was being created and stored properly. I used Insomnia to test my views and create new data. 
​
##### Sales Microservice:
​
| Feature          | URL          |
|:-----------------|:-------------|
|Add a Sales Record|http://localhost:3000/sales|
|List of all customers|http://localhost:3000/customers/|
|Add a customer|http://localhost:3000/customers/new/|
|Add a Sales Person|http://localhost:3000/salesperson/new|
|List of all sales|http://localhost:3000/sales/|
<!-- |List Sales by Sales Person from drop down selection|http://localhost:3000/salesperson/history/| -->


## Sales microservice - Naomi Campos
In the Sales microservice, I have created models for a customer, a salesperson and an automobile value object which is originally created in the "inventory" microservice. A "sale" model with foreign key properties for the other three models mentioned was also created. By utilizing the the RESTful APIs and the Automobile Value Object, we were able to tie each sale to a corresponding automobile. By connecting the microservices this way, we have eliminated the possibility of conflicting "automobile" models and have streamlined the creation of new automobile sales. 
We implemented a poller function to update the sales data every second so the inventory can stay up-to-date.

To ensure we were correctly implementing the "views" that handle the backend functionality, we utilized Insomnia to test every API call.  

The front-end of the application is built with React, the back-end with Django, and the database used is PostgreSQL.
​
Please see below for the ports and URLs to make these API calls:
​
#### Sales Microservice RESTful API calls:
​
| Feature          | Method          | URL          |
|:-----------------|:----------------|:-------------|
|List Sales Records| GET |http://localhost:8090/api/salesrecords/|
|Create Sales Record| POST |http://localhost:8090/api/salesrecords/|
|List Customers| GET |http://localhost:8090/api/customers/|
|Create a Customer| POST |http://localhost:8090/api/customers/|
|List Sales People| GET |http://localhost:8090/api/salespersons/|
|Create a Sales Person| POST |http://localhost:8090/api/salespersons/|


