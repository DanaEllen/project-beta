from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson,
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson,
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer,
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer,
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]


class SalesListEncoder(ModelEncoder):
    model = Sale,
    properties = [
        "id",
        "price",
        "salesperson",
        "customer",
        "automobile"
    ]

    encoders = {
        "salesperson": SalespersonListEncoder(),
        "customer": CustomerListEncoder(),
        "automobile": AutomobileVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonDetailEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Could not create new salesperson"},
                status=400
            )


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
    if request.method == "DELETE":
        Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"message": "Salesperson deleted"})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Could not create new customer"},
                status=400
            )


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    if request.method == "DELETE":
        Customer.objects.filter(id=id).delete()
        return JsonResponse({"message": "Customer deleted"})
    

@require_http_methods(["GET", "POST", "DELETE"])
def api_list_sales(request, auto_vo_id=None):
    if request.method == "GET":
        if auto_vo_id is not None:
            sales = Sale.objects.all(autos=auto_vo_id)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            match_vin = content["automobile"]["vin"]
            auto = AutomobileVO.objects.get(vin=match_vin)
            content["automobile"] = auto
            
            customer_id = content["customer"]["id"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            employee_id = content["salesperson"]["id"]
            salesperson = Salesperson.objects.get(id=employee_id)
            content["salesperson"] = salesperson

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=400
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=400
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        Sale.objects.filter(id=id).delete()
        return JsonResponse({"message": "Sale record deleted"})
